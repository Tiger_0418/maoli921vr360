﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToonLight : MonoBehaviour
{
    private Light lit = null;

    // Start is called before the first frame update
    void Start()
    {
        lit = this.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        Shader.SetGlobalVector("_ToonLightDirection",-this.transform.forward);
    }
}
